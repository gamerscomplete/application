package application

import (
	"gitlab.com/g3n/engine/app"
	"gitlab.com/g3n/engine/gls"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/renderer"
	"time"

	"gitlab.com/g3n/engine/camera"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/util"
	"gitlab.com/g3n/engine/util/logger"
	"gitlab.com/g3n/engine/util/stats"
	"gitlab.com/g3n/engine/window"
)

type App struct {
	*app.Application
	log        *logger.Logger
	dataDir    string
	root       *core.Node
	scene      *core.Node
	frameRater *util.FrameRater
	camera     *camera.Camera

	updateFuncs []func(*App, time.Duration)

	stats *stats.Stats // statistics object
}

var globalApp *App

func (app *App) DirData() string {
	return app.dataDir
}

func GetApp() *App {
	return globalApp
}

func CreateApp(name string, targetFPS uint) *App {
	a := new(App)
	a.Application = app.App(name)
	a.log = logger.New(name, nil)
	a.log.AddWriter(logger.NewConsole(false))
	a.log.SetFormat(logger.FTIME | logger.FMICROS)
	a.log.SetLevel(logger.DEBUG)
	a.stats = stats.NewStats(a.Gls())
	a.updateFuncs = make([]func(*App, time.Duration), 0)
	a.root = core.NewNode()
	a.scene = core.NewNode()
	a.dataDir = "assets"

	// Create camera and orbit control
	width, height := a.GetSize()
	aspect := float32(width) / float32(height)
	a.camera = camera.New(aspect)
	a.scene.Add(a.camera) // Add camera to scene (important for audio demos)

	//Set the distance the camera renders to
	a.camera.SetFar(10240)

	a.frameRater = util.NewFrameRater(targetFPS)
	a.Gls().ClearColor(0.6, 0.6, 0.6, 1.0)

	// Sets the default window resize event handler
	a.Subscribe(window.OnWindowSize, func(evname string, ev interface{}) { a.OnWindowResize() })
	a.OnWindowResize()
	globalApp = a
	return a
}

func (a *App) SwitchScene(scene *core.Node) {
	a.root.Remove(a.scene)
	a.root.Add(scene)
	a.scene = scene
	gui.Manager().Set(a.scene)
}

func (a *App) Root() *core.Node {
	return a.root
}

func (a *App) SubscribeToUpdate(updateFunc func(*App, time.Duration)) {
	a.updateFuncs = append(a.updateFuncs, updateFunc)
}

// OnWindowResize is default handler for window resize events.
func (a *App) OnWindowResize() {

	// Get framebuffer size and set the viewport accordingly
	width, height := a.GetFramebufferSize()
	a.Gls().Viewport(0, 0, int32(width), int32(height))

	// Set camera aspect ratio
	a.camera.SetAspect(float32(width) / float32(height))
}

// Run runs the application render loop
func (a *App) Run() {
	a.Application.Run(a.Update)
}

func (a *App) Update(rend *renderer.Renderer, deltaTime time.Duration) {

	for _, value := range a.updateFuncs {
		value(a, deltaTime)
	}

	// Start measuring this frame
	a.frameRater.Start()

	// Clear the color, depth, and stencil buffers
	a.Gls().Clear(gls.COLOR_BUFFER_BIT | gls.DEPTH_BUFFER_BIT | gls.STENCIL_BUFFER_BIT) // TODO maybe do inside renderer, and allow customization

	// Render scene
	err := rend.Render(a.scene, a.camera)
	if err != nil {
		panic(err)
	}

	// Update GUI timers
	gui.Manager().TimerManager.ProcessTimers()

	// Control and update FPS
	a.frameRater.Wait()
}

// Scene returns the current application 3D scene
func (a *App) Scene() *core.Node {

	return a.scene
}

// Camera returns the current application camera
func (a *App) Camera() *camera.Camera {

	return a.camera
}
